#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for (Node *p; !isEmpty();) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head);
	if (tail == 0)
	{
		tail = head;
	}
}
void List::pushToTail(char el)
{
	tail = new Node(el, tail);
	if (head == 0)
	{
		head = tail;
	}
	//TO DO!
	//TO DO!
	//TO DO!
}
char List::popHead()
{
	char el = head->data;
	Node *tmp = head;
	if (head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
	}
	delete tmp;
	return el;
}
char List::popTail()
{
	int a = tail->data;
	Node *tmp;
	if (!isEmpty())
	{
		tmp = tail->next;
		delete tail;
		tail = tmp;

	}

	// TO DO!
	return NULL;
}
bool List::search(char el)
{
	Node* tmp = head;
	while (tmp != NULL)
	{
		if (tmp->data == el)
		{
			return true;
		} 
		tmp = tmp->next;
	}


	return false;
	// TO DO! (Function to return True or False depending if a character is in the list.
}
void List::reverse()
{
	int count = 0;

	List temp;
	for (Node*tmp = head; tmp != NULL; tmp = tmp->next) {

		temp.pushToHead(tmp->data);
		count++;
	}

	for (Node*tmp = head; tmp != NULL; tmp = tmp->next) {
		pushToHead(temp.popTail());
	}

	for (int i = 0; i < count; i++) {

		popTail();
	}


	// TO DO! (Function is to reverse the order of elements in the list.
}
void List::print()
{
	if (head == tail)
	{
		cout << head->data;
	}
	else
	{
		Node *tmp = head;
		while (tmp != tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}